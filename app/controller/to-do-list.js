const crypto = require('crypto');
const fs = require('fs');

module.exports = {
  saveNewToDoList (req, res) {
    let changed_item_obj = JSON.parse(req.body.changed_item_obj);
    let password = req.body.password;
    // console.log(changed_item_obj);
    // console.log(password);

    let { salt, hash } = saltHashPassword({ password });
    // console.log(salt);
    // console.log(hash);

    let json_data = {
      salt: salt,
      hash: hash,
      data: changed_item_obj
    };
    // console.log(json_data);

    let file_name = randomString(64);
    // console.log(file_name);
    file_name = checkFileExist(file_name);
    // console.log(file_name);

    fs.writeFile(`././data/${file_name}.json`, JSON.stringify(json_data), function (err) {

    });

    res.send(file_name);
  },
  openToDoList (req, res) {
    let file_name = req.params.file_name;

    fs.readFile(`././data/${file_name}.json`, 'utf8' , (err, data) => {
      if (err) {
        res.redirect('/');
      } else {
        req.session.file_name = file_name;
        req.session.to_do_json_data = JSON.parse(data);

        res.redirect('/to-do-list');
      }
    })
  },
  toDoList (req, res) {
    if (req.session.to_do_json_data) {
      res.render('to-do-list', {to_do_json_data: req.session.to_do_json_data});
    } else {
      res.redirect('/');
    }
  },
  checkToDoListPassword (req, res) {
    // console.log(req.body.check_password);

    let to_do_json_data = req.session.to_do_json_data;
    // console.log(to_do_json_data);

    const { hash } = saltHashPassword({
      password: req.body.check_password,
      salt: to_do_json_data.salt
    })

    if (hash === to_do_json_data.hash) {
      res.send(true);
    } else {
      res.send(false);
    }
  },
  updateToDoList (req, res) {
    let changed_item_obj = JSON.parse(req.body.changed_item_obj);
    let removed_item_arr = JSON.parse(req.body.removed_item_arr);
    // console.log(changed_item_obj);
    // console.log(removed_item_arr);

    let to_do_json_data = req.session.to_do_json_data;
    let to_do_json_data_data = req.session.to_do_json_data.data;
    // console.log(to_do_json_data_data);

    for (var tr_id in changed_item_obj) {
      for (var key in changed_item_obj[tr_id]) {
        to_do_json_data_data[tr_id] = {
          ...to_do_json_data_data[tr_id],
          [key]: changed_item_obj[tr_id][key]
        }
      }
    }
    // console.log(to_do_json_data_data);

    for (var i = 0; i < removed_item_arr.length; i++) {
      delete to_do_json_data.data[`tr_${removed_item_arr[i]}`];
    }
    // console.log(to_do_json_data);

    to_do_json_data.data = to_do_json_data_data;
    // console.log(to_do_json_data);

    fs.writeFile(`././data/${req.session.file_name}.json`, JSON.stringify(to_do_json_data), function (err) {

    });

    res.send(true);
  },
  previousToDoListLink (req, res) {
    let file_name_arr = [];
    fs.readdir(`././data/`, (err, files) => {
      files.forEach(file => {
        if (file.includes('.json')) {
          // console.log(file);
          file_name_arr.push(file.slice(0, -5));
        }
      });

      res.render('previous-to-do-list-link', {file_name_arr: file_name_arr});
    });
  }
}

function saltHashPassword ({
  password,
  salt = randomString(4)
}) {
  const hash = crypto
    .createHmac('sha512', salt)
    .update(password)
  return {
    salt,
    hash: hash.digest('hex')
  }
}
function randomString (length) {
  return crypto.randomBytes(length).toString('hex')
}

function checkFileExist(file_name) {
  // console.log(file_name);
  // Check file exist
  if (fs.existsSync(`././data/${file_name}.json`)) {
    // Repeat file name
    return checkFileExist(randomString(64));
  } else {
    return file_name;
  }
}
