const err404Handling = require('./err-404-handling');
const toDoList = require('./to-do-list');

module.exports = {
  err404Handling: err404Handling.err404Handling,

  saveNewToDoList: toDoList.saveNewToDoList,
  openToDoList: toDoList.openToDoList,
  toDoList: toDoList.toDoList,
  checkToDoListPassword: toDoList.checkToDoListPassword,
  updateToDoList: toDoList.updateToDoList,
  previousToDoListLink: toDoList.previousToDoListLink
}
