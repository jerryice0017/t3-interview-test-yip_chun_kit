module.exports = {
  err404Handling (req, res) {
    // Input not exist url -> redirect to homepage
    res.redirect('/');
  }
}
