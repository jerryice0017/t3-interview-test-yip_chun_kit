const appController = require('../app/controller/controller');

module.exports = app => {
  app.get('/', (req, res) => {
    res.render('index');
  });

  app.post('/save-new-to-do-list', appController.saveNewToDoList);

  app.get('/to-do-list&fn=:file_name', appController.openToDoList);

  app.get('/to-do-list', appController.toDoList);

  app.post('/check-to-do-list-password', appController.checkToDoListPassword);

  app.post('/update-to-do-list', appController.updateToDoList);

  app.get('/previous-to-do-list-link', appController.previousToDoListLink);

  // err-404-handling
  app.get('*', appController.err404Handling);
}
