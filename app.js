const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
var http = require('http');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const routes = require('./routes/index');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(session({
  secret: "b5d9dbd5069e5b0cefdc0d1437ddfe84c52365366dc086f4517c9a8b1266d8762dab4e8fe92ae7325644acca42d8546555328069da9c18799215ffc20ba066a8",
  resave: true,
  saveUninitialized: true
}));


app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

//設置路由引入
routes(app);

const port = 3000;
app.listen(port, () => console.log(`Server running on port ${port}`));
