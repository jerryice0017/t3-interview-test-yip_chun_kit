Requirements:
- Install npm (https://nodejs.org/en/download/)

How to start the program
1. Open cmd
2. change directory to this folder
3. type "npm install"
4. type 'node app.js'
5. open a browser and enter "http://localhost:3000"
